package com.example.carolina.moviesmobile;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Fragment_login.OnFragmentInteractionListener} interface
 * to handle interaction events.
 */
public class Fragment_login extends Fragment {

    private OnFragmentInteractionListener mListener;

    public Fragment_login() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_fragment_login, container, false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    public void irAcceso(View v){
        //  ACCESO LOGIN (NO FUNCIONA)
        EditText et_usuario= (EditText) getActivity().findViewById(R.id.et_usuario);
        EditText et_pass=(EditText)getActivity().findViewById(R.id.et_pass);


        String usuario = et_usuario.getText().toString();
        String pass =et_pass.getText().toString();

        Intent i = new Intent(this.getActivity(), AccesoActiviy.class); // de donde vengo a donde voy
        i.putExtra("usuario",usuario);
        //put extra nombre de como recibe, el valor a enviar
        i.putExtra("pass",pass);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); this.getActivity().finish();
        startActivity(i);
        this.getActivity().finish();

    }
}
