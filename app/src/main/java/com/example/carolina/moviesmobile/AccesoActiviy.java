package com.example.carolina.moviesmobile;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

/**
 * Created by alexroo on 21/09/2016.
 */
public class AccesoActiviy extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.acceso_layout);

        Bundle parametros = getIntent().getExtras();
        //getintent obtener de la actividad que nos llamo los parametros que envia, get extras obtener
        String usu = parametros.getString("usuario");
        String pass= parametros.getString("pass");

        TextView usuarioRecibido = (TextView) findViewById(R.id.tv_mostrar);
        usuarioRecibido.setText(usu);

        TextView passR =(TextView) findViewById(R.id.tv_mostrarPass);
        passR.setText(pass);

    }
}
